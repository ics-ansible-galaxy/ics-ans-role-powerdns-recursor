class FilterModule(object):
    def filters(self):
        return {'reverse_host_overwrite': self.reverse_host_overwrite,
                'generate_forward_config': self.generate_forward_config
                }

    def reverse_host_overwrite(self, hosts_dict):
        ret = {}
        for host in hosts_dict.keys():
            ip = hosts_dict[host]
            ip_obj = ret.get(ip, [])
            ip_obj.append(host)
            ret[ip] = ip_obj
        return ret

    def generate_forward_config(self, my_hostvars, domains, masters):
        """
        This function generate the content of the forwarding file list
        """
        if not isinstance(domains, list):
            raise Exception('domains must be a List')
        if not isinstance(masters, list):
            raise Exception('masters must be a List')
        subnets = []
        for hostvar in my_hostvars.values():
            for interface in hostvar.get("csentry_interfaces", []):
                ip = interface.get("ip", None)
                domain = interface.get("network", {}).get("domain", None)
                if domain is None or domain not in domains:
                    continue
                if ip is None:
                    continue
                ipS = ip.split('.')
                reverse = "{0}.{1}.{2}.in-addr.arpa.".format(ipS[2], ipS[1], ipS[0])
                if reverse not in subnets:
                    subnets.append(reverse)
        m_string = ', '.join(masters)
        lines = [f"{z_line} = {m_string}" for z_line in subnets + domains]
        return "\n".join(lines)
