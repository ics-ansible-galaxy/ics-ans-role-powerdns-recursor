ics-ans-role-powerdns-recursor
===================

Ansible role to install powerdns-recursor.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------
The role install powerDNS recursor with the capability to use a LUA script to customize DNS replies.
The role can manage host overwrite using two different methods:
- LUA scripting
- Simple hosts file

The former uses a more sophisticad mechanism to return CNAMES overwrite.
The latter simply maps the provided host to a A record containing the provided IP.

```yaml

# pdns_rec_config_dns_script_overwrite. The lua script will be deployed only if pdns_rec_overwrite_proxies_lua or pdns_rec_overwrite_hosts_lua are not empty (the default)
pdns_rec_config_dns_script_overwrite: "{{ pdns_rec_config_dir }}/reverseproxy-overwrite.lua"

# these variables are used to overwrite records in a reverse proxy context
# reverse proxy definition using LUA
pdns_rec_overwrite_proxies_lua:
  - bastion-01.esss.lu.se: "1.2.3.4"
  - bastion-02.esss.lu.se: "2.3.4.5"

# Host rewrite
pdns_rec_overwrite_hosts_lua:
  - site-01.esss.lu.se: "bastion-01.esss.lu.se"
  - site-02.esss.lu.se: "bastion-02.esss.lu.se"

# Simple host overwrite
pdns_rec_overwrite_hosts:
  tardis: 10.3.42.2
  www.google.it: 10.3.42.2
  anotherhost: 10.3.42.6


# DNS queries logging
pdns_rec_enable_query_log: true   # enable log queries in journald

# wildcard overwrite
pdns_rec_enable_wildcard: true    # enable log queries and act as a wildcard DNS for the specified (sub)domain
pdns_rec_wildcard_suffix: "overwrite.ess.eu"     # specify for which sub-domain we want the wildcard behaviour
pdns_rec_wildcard_host: "host1"        # host A record associaed to the wildcard behaviour
pdns_rec_wildcard_ip: "1.1.1.1"        # which IP will be return by the DNS for the wildcard behaviour 

```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-powerdns-recursor
```

License
-------

BSD 2-clause
