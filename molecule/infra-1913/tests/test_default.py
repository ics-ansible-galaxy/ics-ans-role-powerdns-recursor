import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('recursors')


def test_pdns_package(host):
    p = host.package("pdns-recursor")
    assert p.is_installed


def test_pdns_conffile(host):
    f = host.file("/etc/pdns-recursor/recursor.conf")
    assert f.exists
    assert f.is_file


def test_pdns_service(host):
    s = host.service("pdns-recursor")
    assert s.is_running
    assert s.is_enabled


def test_pdns_port(host):
    # print(host.ansible.get_variables())
    # localip = host.ansible.get_variables()['ansible_default_ipv4']['address']
    assert host.socket("udp://127.0.0.1:53").is_listening


def test_pdns_zones(host):
    f = host.file("/etc/pdns_forward_zones.txt")
    assert f.exists
    assert f.is_file
    assert f.contains('bms.esss.lu.se')
