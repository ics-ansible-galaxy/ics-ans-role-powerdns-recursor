import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('hosts_overwrite')


def test_hosts_overwrite(host):
    cmd = host.run("host www.google.it 127.0.0.1")
    assert cmd.rc == 0
    assert '10.3.42.2' in cmd.stdout
