import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('log_wildcard')


def test_hosts_overwrite(host):
    cmd = host.run("host whatever.overwrite.ess.eu  127.0.0.1")
    assert cmd.rc == 0
    assert '1.1.1.1' in cmd.stdout
